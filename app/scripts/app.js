(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'profile': '/vista-usuario',
      'new-user': '/alta-cliente',
      'transactions': '/movimientos',
      'operations': '/operaciones',
      'logout': '/logout',
      'currency-exchange':'/CambioDivisa',
      'error' : '/error'
    }
  });

}(document));
